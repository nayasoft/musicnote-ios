
var clientId = '108836214954.apps.googleusercontent.com';
var apiKey = 'AIzaSyBOtloWHUHXU9qb2AhF2Uba0bNMLRFS0CY';
var scopes = 'https://www.googleapis.com/auth/calendar';
//var scopes = 'https://www.google.com/calendar/feeds/';

function handleClientLoad() {
  gapi.client.setApiKey(apiKey);
  window.setTimeout(checkAuth,1);
  checkAuth();
}

function checkAuth() {
  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true},
      handleAuthResult);
}

function handleAuthResult(authResult) {
  //var authorizeButton = document.getElementById('authorize-button');
  if (authResult) {
    console.log('User is authorized to process!');
    //makeApiCall();
  } else {
   console.log('User not authorized for calendar access!');
   }
}

function handleAuthClick(event) {
  gapi.auth.authorize(
      {client_id: clientId, scope: scopes, immediate: false},
      handleAuthResult);
  return false;
}

function makeApiCall() {
  gapi.client.load('calendar', 'v3', function() {
    var request = gapi.client.calendar.events.list({
      'calendarId': 'ajug02s97nu6hisao14ermrohc@group.calendar.google.com'
    });
          
    request.execute(function(resp) {
      for (var i = 0; i < resp.items.length; i++) {
        var li = document.createElement('li');
		var eventHead=resp.items[i].summary;
        //li.appendChild(document.createTextNode(resp.items[i].summary));
		//document.getElementById('events').appendChild(li);		
      }	
    });
  });
}

 var resource = {
        "summary": "Adding new event",
        "location": "Somewhere",
        "start": {
          "dateTime": "2013-05-29T10:00:00.000-07:00"
        },
        "end": {
          "dateTime": "2013-05-29T10:25:00.000-08:00"
        }
      };

	function makeRpcRequest() {
	   gapi.client.load('calendar', 'v3', function() {
			var request = gapi.client.calendar.events.insert({
			  'calendarId': 'ajug02s97nu6hisao14ermrohc@group.calendar.google.com',
			  'resource': resource
			});
			request.execute(writeResponse);
		});
      }

function writeResponse(response) {
        console.log(response);
		$('#calendar').fullCalendar('refresh');
        /*var creator = response.creator.email;
        var calendarEntry = response.htmlLink;
        var infoDiv = document.getElementById('info');
        var infoMsg = document.createElement('P');
        infoMsg.appendChild(document.createTextNode('Calendar entry ' +
            'successfully created by ' + creator));
        infoDiv.appendChild(infoMsg);
        var entryLink = document.createElement('A');
        entryLink.href = calendarEntry;
        entryLink.appendChild(
            document.createTextNode('View the Calendar entry'));
        infoDiv.appendChild(entryLink);*/
      }
/*
$.getJSON("http://www.google.com/calendar/feeds/developer-calendar@google.com/public/full?alt=json", function(data, textStatus){
    $.each(data.feed.entry, function(index, event){
        if(event.gd$when) {
            var event_date = $.datepicker.formatDate('yymmdd', new Date(event.gd$when[0].startTime));
            if (!(calendar_data.hasOwnProperty(event_date))) {
                calendar_data[event_date] = new Array();
            }
            event.gd$when[0].formatedTime = $.datepicker.formatDate('MM d, yy', new Date(event.gd$when[0].startTime));
            calendar_data[event_date].push(event);
        }
    });
    showEvent(new Date());

    $('#calendar-datepicker').datepicker("refresh");
});
*/  
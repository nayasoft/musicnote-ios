//userId=1002;
var videoDeleted=false;
var fileDeleted=false;
var fileDownloadedName="";
$(document).ready( function() {
	 recordedLessons();
	 otherFiles();
	/*$('#fileuploaddiv').find('.ajax-upload-dragdrop').each(function(index){
		$(this).removeAttr('.ajax-upload-dragdrop');
	});*/

	$('#myForm').submit(function ()
			{
		
		       var url = urlForServer+'Upload/file/'+userId+'/'+musicnote;
		       $(this).attr('action', url);
		      // recordedLessons();
		      
			});



var options = { 
beforeSend: function() 
{
	$("#progress").show();
	//clear everything
	$("#bar").width('0%');
	$("#message").html("");
	$("#percent").html("0%");
	
},
uploadProgress: function(event, position, total, percentComplete) 
{
	$("#bar").width(percentComplete+'%');
	$("#percent").html(percentComplete+'%');

},
success: function() 
{
	$("#bar").width('100%');
	$("#percent").html('100%');
	 otherFiles();

},
complete: function(response) 
{
	$("#myfile").val("");
	setTimeout(function(){
		$('#progress').hide();	
	},500);
},
error: function()
{

}

}; 

 $("#myForm").ajaxForm(options);

	/*var settings = {
			headers: { 
				"Mn-Callers" : musicnote,
				"Mn-time" :musicnoteIn				
				},
			url:urlForServer+'Upload/file/'+userId+'/'+musicnote,
			method: "POST",
			//allowedTypes:"jpg,png,gif,doc,pdf,zip",
			multiple: true,
			onSuccess:function(files,data,xhr)
			{
			$("#status").html("<font color='green'>Upload is success</font>");



			},
			onError: function(files,status,errMsg)
			{      
			$("#status").html("<font color='red'>Upload is Failed</font>");
			}
			}

	$("#mulitplefileuploader").uploadFile(settings);*/


	// Fixing height of the page.
	var size=$(window).height() - 90;
    $('#recordLessonView').attr('style','height:'+size+'px;overflow: auto;');
	
                  
				  $("ul#viewRecordedLesson").on('click','.glyphicon-download-alt',function() {
					if(!videoDeleted)
						getFileFor($(this).parent().attr('id'));
					else
						videoDeleted=false;
					});
	             $("ul#viewOtherFiles").on('click','.glyphicon-download-alt',function() {
					if(!fileDeleted)
						//getFileFor(this.lastElementChild.id);
						downLoadFile($(this).parent().attr('id'));
					else
						fileDeleted=false;
					});
			  
				  });
				  
				    $('ul#viewRecordedLesson').on('click','.playRecorded',function(){
	         			var lessonName = $(this).attr('id');
	         			if(lessonName.contains(".mov") || lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv")  || lessonName.contains(".wma"))
	         			{
	         					$('ul#viewRecordedLesson').children('li').children('video').each(function( index ){
	         						$('ul#viewRecordedLesson').children('li').children('video').remove();
	         					});
								$('ul#viewRecordedLesson').children('li').children('embed').each(function( index ){
         						   $('ul#viewRecordedLesson').children('li').children('embed').remove();
         					      });
		         			
		         			
		         			$(this).after('<video  width="80%" height="40%"  id="player1" autoplay="autoplay"    type="video/mov" width=320 height=240><source src="'+uploadUrl+lessonName+'" type="video/mp4"></source></video>');
		         			
	         			}
	         			else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
	         			{
         					$('ul#viewRecordedLesson').children('li').children('video').each(function( index ){
	         						$('ul#viewRecordedLesson').children('li').children('video').remove();
	         					});
							$('ul#viewRecordedLesson').children('li').children('embed').each(function( index ){
         						$('ul#viewRecordedLesson').children('li').children('embed').remove();
         					});
	         				
		         			$(this).after('<embed  height="50" width="180" name="plugin" src="'+uploadUrl+lessonName+'" type="audio/x-wav" controls AUTOSTART="false"/> ');
		         			
	         			}
	         		
	         	});
	             
	             
	             $('ul#viewOtherFiles').on('click','.playRecorded',function(){
	         			var lessonName = $(this).attr('id');
	         			
	         			if(lessonName.contains(".mov") || lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv")  || lessonName.contains(".wma"))
	         			{
	         				$('ul#viewOtherFiles').children('li').children('video').each(function( index ){
         						$('ul#viewOtherFiles').children('li').children('video').remove();
         					});
							$('ul#viewOtherFiles').children('li').children('embed').each(function( index ){
	         						$('ul#viewOtherFiles').children('li').children('embed').remove();
	         					});
	         				
		         			$(this).after('<video  width="80%" height="40%"  id="player1" autoplay="autoplay"    type="video/mov" width=320 height=240><source src="'+uploadUrl+lessonName+'" type="video/mp4"></source></video>');
	         			}
	         			else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
	         			{
	         				$('ul#viewOtherFiles').children('li').children('video').each(function( index ){
	         						$('ul#viewOtherFiles').children('li').children('video').remove();
	         					});
         					$('ul#viewOtherFiles').children('li').children('embed').each(function( index ){
         						$('ul#viewOtherFiles').children('li').children('embed').remove();
         					});
	         				
		         			$(this).after('<embed  height="50" width="180" name="plugin" src="'+uploadUrl+lessonName+'" type="audio/x-wav" controls AUTOSTART="false"/> ');
		         			
	         			
	         			}
	         		
	         	});
 

function recordedLessons()
{
	var url = urlForServer+"Lesson/getRecordedLessons/"+userId;
	$.support.cors = true;
	$.ajax({
		headers: { 
			"Mn-Callers" : musicnote,
			"Mn-time" :musicnoteIn				
			},
           type : 'POST',
           url : url,
           success : function(responseText) {
           console.log("<-------Recorded lessons received with response as -------> "
                + responseText);
           
           var data=jQuery.parseJSON(responseText);
           $("#viewRecordedLesson").empty();
           if(data!=null && data!='')
           {
          
           for ( var i = 0; i < data.length; i++) {
			   var obj = data[i];
			   var lessonName = obj['fileName'];
			   var fileDummyName=obj['fileDummyName'];
			   
			   var uploadDate=obj['uploadDate'];
			   var playOption=obj['fullPath'];
			    if(lessonName.contains(".mov") || lessonName.contains(".mp4") || lessonName.contains(".avi") || lessonName.contains(".wmv")  || lessonName.contains(".wma"))
			   
			   {
					 $("#viewRecordedLesson").append('<li><a id="'+playOption+'" class="playRecorded" href="javascript:void(0);"><i class="glyphicon glyphicon-download-alt ~' +lessonName+'" title="Click to download"></i>&nbsp;' + fileDummyName + '<i  class="offset2">'+uploadDate+'</i><span id="'+lessonName+'" class="close"  >&times;</span> </a> </li>');
  
			   }
			   else if(lessonName.contains(".mp3") || lessonName.contains(".wav") || lessonName.contains(".amr"))
				   {
						$("#viewRecordedLesson").append('<li><a id="'+playOption+'" class="playRecorded" href="javascript:void(0);"><i class="glyphicon glyphicon-download-alt ~' +lessonName+'" title="Click to download"></i>&nbsp;' + fileDummyName + '<i  class="offset2">'+uploadDate+'</i><span id="'+lessonName+'" class="close"  >&times;</span> </a> </li>');

				   }
			   else
			   {
				   
				// $("#viewRecordedLesson").append('<li><a id="'+lessonName+'" href="javascript:void(0);">' + lessonName + ' <span id="'+lessonName+'" class="close"  >&times;</span> </a><video  id="client-video"  loop controls src="http://musicnoteapp.com/UploadMusicFiles/1002/video/Nov-26-201322-43-01_test.mov "  type="video/mov" width=320 height=240></video> </li>');
			  $("#viewRecordedLesson").append('<li><a id="'+playOption+'" href="javascript:void(0);">' + fileDummyName + '<i  class="offset2">'+uploadDate+'</i><span id="'+lessonName+'" class="close"  >&times;</span> </a></li>');
			   }
           }
           }
           },
           error : function() {
           console.log("<-------error returned retrieving recorded lessons-------> ");
           }
           });
}

function getFileFor(fileName){
		var fileExt=fileName.split('.');
		var	ext = fileExt[fileExt.length-1];
	   var href=urlForServer+"Lesson/download/"+userId+'/'+fileName+'/'+ext;
	   var remoteFile = downloadUrl+fileName;
		var localFileName = remoteFile.substring(remoteFile.lastIndexOf('/')+1);
		
		fileDownloadedName=remoteFile;
	    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFileSystemSuccess,fail);
		}
		
		
function onFileSystemSuccess(fileSystem)
{
	    	//fileSystem.root.getFile(localFileName, {create: true, exclusive: false}, function(fileEntry) {
	    	var localPath = fileSystem.root.fullPath+"/"+localFileName;
			
	    	if (device.platform === "Android" && localPath.indexOf("file://") === 0) {
	    		localPath = localPath.substring(7);
	    	}
	    	
	    	var fileTransfer = new FileTransfer();
	    	fileTransfer.download(
	    		fileDownloadedName,
	    		localPath,
	    		function(entry) {
	    			alert("download complete: " + entry.fullPath);
	    		},
	    		function(error) {
	    			alert("Download error "+error.source);
					alert("Download error "+error.source);
					
	    		});	
	    
}


function downLoadFile(fileName)
{

 //alert(fileName); 
        var fileExt=fileName.split('.');
		var	ext = fileExt[fileExt.length-1];
	   var uri=urlForServer+"Lesson/download/"+userId+'/'+fileName+'/'+ext; 
        var fileTransfer = new FileTransfer();
     
        var filePath = "/mnt/sdcard/"+fileExt;
		alert(filePath);
        fileTransfer.download(
            uri,
            filePath,
            function(entry) {
                alert("download complete: " + entry.fullPath);
            },
            function(error) {
                alert("download error source " + error.source);
                alert("download error target " + error.target);
                alert("upload error code" + error.code);
            },
            true,
            {
            }
        );
}
function otherFiles()
{
	var url = urlForServer+"Lesson/getOtherFiles/"+userId;
	$.support.cors = true;
	$.ajax({
		headers: { 
			"Mn-Callers" : musicnote,
			"Mn-time" :musicnoteIn				
			},
           type : 'POST',
           url : url,
           success : function(responseText) {
           console.log("<-------Files received with response as -------> "
                + responseText);
           
           var data=jQuery.parseJSON(responseText);
           $("#viewOtherFiles").empty();
           if(data!=null && data!='')
           {
          
           for ( var i = 0; i < data.length; i++) {
			   var obj = data[i];
			   var fileName = obj['fileName'];
			    var originalFileName=obj['fileDummyName'];
			    var uploadDate=obj['uploadDate'];
			    var playOption=obj['fullPath'];
			   if(fileName.contains(".mov") ||  fileName.contains(".mp4") || fileName.contains(".avi") || fileName.contains(".wmv") || fileName.contains(".wma") )
			   {
				   $("#viewOtherFiles").append('<li><a id="'+playOption+'" class="playRecorded" href="javascript:void(0);"><i class="glyphicon glyphicon-download-alt ~' +fileName+'" title="Click to download"></i>&nbsp;' + originalFileName + '<i  class="offset2">'+uploadDate+'</i><span id="'+fileName+'" class="close">&times;</span> </a> </li>');

				   //$("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);">' + fileName + ' <span id="'+fileName+'" class="close">&times;</span> </a><video width="80%" height="40%"  id="player1" autoplay="autoplay" controls="controls" preload="none"><source src="src="'+uploadUrl+userId+"/"+uploadUrl2+fileName+'" type="video/mp4"></source></video></li>');

			   }
			 
			   else if(fileName.contains(".mp3") || fileName.contains(".wav") || fileName.contains(".amr"))
			   {
				    $("#viewOtherFiles").append('<li><a id="'+playOption+'" class="playRecorded" href="javascript:void(0);"><i class="glyphicon glyphicon-download-alt ~' +fileName+'" title="Click to download"></i>&nbsp;' + originalFileName + '<i  class="offset2">'+uploadDate+'</i><span id="'+fileName+'" class="close">&times;</span> </a></li>');

			   }
			  
			   else
			   {
				   $("#viewOtherFiles").append('<li><a id="'+playOption+'" href="javascript:void(0);"><i class="glyphicon glyphicon-download-alt ~' +fileName+'" title="Click to download"></i>&nbsp;' + originalFileName + '<i  class="offset2">'+uploadDate+'</i><span id="'+fileName+'" class="close">&times;</span> </a> </li>');
				  //  $("#viewOtherFiles").append('<li><a id="'+fileName+'" href="javascript:void(0);">' + fileName + ' <span id="'+fileName+'" class="close">&times;</span> </a><video  id="client-video"  loop controls  width=320 height=240><source src="'+test+'" /></video>  </li>');


			   }

           }
           }
           
           },
           error : function() {
           console.log("<-------error returned retrieving files-------> ");
           }
         });
}

$("ul#viewRecordedLesson").on('click','li span',function() {
	
	deleteVideoFiles(this.id);
	videoDeleted=true;
	});
	
$("ul#viewOtherFiles").on('click','li span',function() {
	
	deleteFiles(this.id);
	fileDeleted=true;
	});
	
	function deleteFiles(fileName)
	{
		var url = urlForServer+"Lesson/deleteVideo/"+fileName+"/"+userId;
		
		$.support.cors = true;
		$.ajax({
			headers: { 
				"Mn-Callers" : musicnote,
				"Mn-time" :musicnoteIn				
				},
				type : 'POST',
				url : url,
				success : function(responseText) {
			    var data=jQuery.parseJSON(responseText);
			         if(data[0].status=='success')
			         {
			        	 
			        	 $('#uploadmsgModal').modal('show');
			     		//$("#loadingImg").attr('src',loadingImgUrl);
			     		$("#uploadmsg-header-span").text("info");
			     		$("#uploadmodal-message").text("  Creating user");
			     		$("#schedulestatusicon").remove();
			     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-time" id="schedulestatusicon"></i>');
			     		$("#uploadmsgfooterbtn").attr('class','btn btn-info');
			     		$("#uploadmsg-header-span").text("info");
			     		$("#uploadmodal-message").text("  File Deleted Successfully");
			     		$("#schedulestatusicon").remove();
			     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-ok" id="schedulestatusicon"></i>');
			     		$("#uploadmsgfooterbtn").attr('class','btn btn-info');
			     		setTimeout(function(){
			     			$('#uploadmsgModal').modal('hide');	
			     		},5000);
					console.log("<-------File deleted successfully -------> "+ responseText);
					recordedLessons();
					otherFiles();
			         }
			         
			         else
			         {
			        	 $('#uploadmsgModal').modal('show');
				     		//$("#loadingImg").attr('src',loadingImgUrl);
				     		$("#uploadmsg-header-span").text("info");
				     		$("#uploadmodal-message").text("  Creating user");
				     		$("#schedulestatusicon").remove();
				     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-time" id="schedulestatusicon"></i>');
				     		$("#uploadmsgfooterbtn").attr('class','btn btn-info');
				     		$("#uploadmsg-header-span").text("info");
				     		$("#uploadmodal-message").text("  File Attached some notes");
				     		$("#schedulestatusicon").remove();
				     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-ok" id="schedulestatusicon"></i>');
				     		$("#uploadmsgfooterbtn").attr('class','btn btn-warning');
				     		setTimeout(function(){
				     			$('#uploadmsgModal').modal('hide');	
				     		},5000); 
			         }
				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});
	}
	function deleteVideoFiles(fileName)
	{
		var url = urlForServer+"Lesson/deleteVideoFiles/"+fileName+"/"+userId;
		
		$.support.cors = true;
		$.ajax({
			headers: { 
				"Mn-Callers" : musicnote,
				"Mn-time" :musicnoteIn				
				},
				type : 'POST',
				url : url,
				success : function(responseText) {
			    var data=jQuery.parseJSON(responseText);
			         if(data[0].status=='success')
			         {
			        	 
			        	 $('#uploadmsgModal').modal('show');
			     		//$("#loadingImg").attr('src',loadingImgUrl);
			     		$("#uploadmsg-header-span").text("info!");
			     		$("#uploadmodal-message").text("  Creating user");
			     		$("#schedulestatusicon").remove();
			     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-time" id="schedulestatusicon"></i>');
			     		$("#uploadmsgfooterbtn").attr('class','btn btn-info');
			     		$("#uploadmsg-header-span").text("info");
			     		$("#uploadmodal-message").text("  File Deleted Successfully");
			     		$("#schedulestatusicon").remove();
			     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-ok" id="schedulestatusicon"></i>');
			     		$("#uploadmsgfooterbtn").attr('class','btn btn-info');
			     		setTimeout(function(){
			     			$('#uploadmsgModal').modal('hide');	
			     		},5000);
					console.log("<-------File deleted successfully -------> "+ responseText);
					recordedLessons();
					otherFiles();
			         }
			         
			         else
			         {
			        	 $('#uploadmsgModal').modal('show');
				     		//$("#loadingImg").attr('src',loadingImgUrl);
				     		$("#uploadmsg-header-span").text("info!");
				     		$("#uploadmodal-message").text("  Creating user");
				     		$("#schedulestatusicon").remove();
				     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-time" id="schedulestatusicon"></i>');
				     		$("#uploadmsgfooterbtn").attr('class','btn btn-info');
				     		$("#uploadmsg-header-span").text("info");
				     		$("#uploadmodal-message").text("  File Attached some notes");
				     		$("#schedulestatusicon").remove();
				     		$("#uploadmsgfooterbtn").prepend('<i class="white glyphicon glyphicon-ok" id="schedulestatusicon"></i>');
				     		$("#uploadmsgfooterbtn").attr('class','btn btn-warning');
				     		setTimeout(function(){
				     			$('#uploadmsgModal').modal('hide');	
				     		},5000); 
			         }
				},
				error : function() {
					console.log("<-------error returned deleting file-------> ");
				}
			});
	}